import request from '@/utils/request'

// 查询专业信息列表
export function listMajors(query) {
  return request({
    url: '/major/majors/list',
    method: 'get',
    params: query
  })
}

// 查询专业信息详细
export function getMajors(majorId) {
  return request({
    url: '/major/majors/' + majorId,
    method: 'get'
  })
}

// 新增专业信息
export function addMajors(data) {
  return request({
    url: '/major/majors',
    method: 'post',
    data: data
  })
}

// 修改专业信息
export function updateMajors(data) {
  return request({
    url: '/major/majors',
    method: 'put',
    data: data
  })
}

// 删除专业信息
export function delMajors(majorId) {
  return request({
    url: '/major/majors/' + majorId,
    method: 'delete'
  })
}

// 导出专业信息
export function exportMajors(query) {
  return request({
    url: '/major/majors/export',
    method: 'get',
    params: query
  })
}