import request from '@/utils/request'

// 查询院校信息列表
export function listManage(query) {
  return request({
    url: '/university/manage/list',
    method: 'get',
    params: query
  })
}

// 查询院校信息详细
export function getManage(universityId) {
  return request({
    url: '/university/manage/' + universityId,
    method: 'get'
  })
}

// 新增院校信息
export function addManage(data) {
  return request({
    url: '/university/manage',
    method: 'post',
    data: data
  })
}

// 修改院校信息
export function updateManage(data) {
  return request({
    url: '/university/manage',
    method: 'put',
    data: data
  })
}

// 删除院校信息
export function delManage(universityId) {
  return request({
    url: '/university/manage/' + universityId,
    method: 'delete'
  })
}

// 导出院校信息
export function exportManage(query) {
  return request({
    url: '/university/manage/export',
    method: 'get',
    params: query
  })
}