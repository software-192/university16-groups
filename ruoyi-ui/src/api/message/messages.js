import request from '@/utils/request'

// 查询高考资讯模块列表
export function listMessages(query) {
  return request({
    url: '/message/messages/list',
    method: 'get',
    params: query
  })
}

// 查询高考资讯模块详细
export function getMessages(messageId) {
  return request({
    url: '/message/messages/' + messageId,
    method: 'get'
  })
}

// 新增高考资讯模块
export function addMessages(data) {
  return request({
    url: '/message/messages',
    method: 'post',
    data: data
  })
}

// 修改高考资讯模块
export function updateMessages(data) {
  return request({
    url: '/message/messages',
    method: 'put',
    data: data
  })
}

// 删除高考资讯模块
export function delMessages(messageId) {
  return request({
    url: '/message/messages/' + messageId,
    method: 'delete'
  })
}

// 导出高考资讯模块
export function exportMessages(query) {
  return request({
    url: '/message/messages/export',
    method: 'get',
    params: query
  })
}