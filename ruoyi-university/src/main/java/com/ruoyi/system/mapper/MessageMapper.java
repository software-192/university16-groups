package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Message;

/**
 * 高考资讯模块Mapper接口
 * 
 * @author ruoyi
 * @date 2021-10-13
 */
public interface MessageMapper 
{
    /**
     * 查询高考资讯模块
     * 
     * @param messageId 高考资讯模块主键
     * @return 高考资讯模块
     */
    public Message selectMessageByMessageId(Integer messageId);

    /**
     * 查询高考资讯模块列表
     * 
     * @param message 高考资讯模块
     * @return 高考资讯模块集合
     */
    public List<Message> selectMessageList(Message message);

    /**
     * 新增高考资讯模块
     * 
     * @param message 高考资讯模块
     * @return 结果
     */
    public int insertMessage(Message message);

    /**
     * 修改高考资讯模块
     * 
     * @param message 高考资讯模块
     * @return 结果
     */
    public int updateMessage(Message message);

    /**
     * 删除高考资讯模块
     * 
     * @param messageId 高考资讯模块主键
     * @return 结果
     */
    public int deleteMessageByMessageId(Integer messageId);

    /**
     * 批量删除高考资讯模块
     * 
     * @param messageIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMessageByMessageIds(Integer[] messageIds);
}
