package com.ruoyi.system.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Message;
import com.ruoyi.system.service.IMessageService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 高考资讯模块Controller
 * 
 * @author ruoyi
 * @date 2021-10-13
 */
@RestController
@RequestMapping("/message/messages")
public class MessageController extends BaseController
{
    @Autowired
    private IMessageService messageService;

    /**
     * 查询高考资讯模块列表
     */
    @PreAuthorize("@ss.hasPermi('message:messages:list')")
    @GetMapping("/list")
    public TableDataInfo list(Message message)
    {
        startPage();
        List<Message> list = messageService.selectMessageList(message);
        return getDataTable(list);
    }

    /**
     * 导出高考资讯模块列表
     */
    @PreAuthorize("@ss.hasPermi('message:messages:export')")
    @Log(title = "高考资讯模块", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Message message)
    {
        List<Message> list = messageService.selectMessageList(message);
        ExcelUtil<Message> util = new ExcelUtil<Message>(Message.class);
        return util.exportExcel(list, "高考资讯模块数据");
    }

    /**
     * 获取高考资讯模块详细信息
     */
    @PreAuthorize("@ss.hasPermi('message:messages:query')")
    @GetMapping(value = "/{messageId}")
    public AjaxResult getInfo(@PathVariable("messageId") Integer messageId)
    {
        return AjaxResult.success(messageService.selectMessageByMessageId(messageId));
    }

    /**
     * 新增高考资讯模块
     */
    @PreAuthorize("@ss.hasPermi('message:messages:add')")
    @Log(title = "高考资讯模块", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Message message)
    {
        return toAjax(messageService.insertMessage(message));
    }

    /**
     * 修改高考资讯模块
     */
    @PreAuthorize("@ss.hasPermi('message:messages:edit')")
    @Log(title = "高考资讯模块", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Message message)
    {
        return toAjax(messageService.updateMessage(message));
    }

    /**
     * 删除高考资讯模块
     */
    @PreAuthorize("@ss.hasPermi('message:messages:remove')")
    @Log(title = "高考资讯模块", businessType = BusinessType.DELETE)
	@DeleteMapping("/{messageIds}")
    public AjaxResult remove(@PathVariable Integer[] messageIds)
    {
        return toAjax(messageService.deleteMessageByMessageIds(messageIds));
    }
}
