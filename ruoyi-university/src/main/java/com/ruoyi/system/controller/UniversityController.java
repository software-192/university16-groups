package com.ruoyi.system.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.University;
import com.ruoyi.system.service.IUniversityService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 院校信息Controller
 * 
 * @author ruoyi
 * @date 2021-10-13
 */
@RestController
@RequestMapping("/university/manage")
public class UniversityController extends BaseController
{
    @Autowired
    private IUniversityService universityService;

    /**
     * 查询院校信息列表
     */
    @PreAuthorize("@ss.hasPermi('university:manage:list')")
    @GetMapping("/list")
    public TableDataInfo list(University university)
    {
        startPage();
        List<University> list = universityService.selectUniversityList(university);
        return getDataTable(list);
    }

    /**
     * 导出院校信息列表
     */
    @PreAuthorize("@ss.hasPermi('university:manage:export')")
    @Log(title = "院校信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(University university)
    {
        List<University> list = universityService.selectUniversityList(university);
        ExcelUtil<University> util = new ExcelUtil<University>(University.class);
        return util.exportExcel(list, "院校信息数据");
    }

    /**
     * 获取院校信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('university:manage:query')")
    @GetMapping(value = "/{universityId}")
    public AjaxResult getInfo(@PathVariable("universityId") Long universityId)
    {
        return AjaxResult.success(universityService.selectUniversityByUniversityId(universityId));
    }

    /**
     * 新增院校信息
     */
    @PreAuthorize("@ss.hasPermi('university:manage:add')")
    @Log(title = "院校信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody University university)
    {
        return toAjax(universityService.insertUniversity(university));
    }

    /**
     * 修改院校信息
     */
    @PreAuthorize("@ss.hasPermi('university:manage:edit')")
    @Log(title = "院校信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody University university)
    {
        return toAjax(universityService.updateUniversity(university));
    }

    /**
     * 删除院校信息
     */
    @PreAuthorize("@ss.hasPermi('university:manage:remove')")
    @Log(title = "院校信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{universityIds}")
    public AjaxResult remove(@PathVariable Long[] universityIds)
    {
        return toAjax(universityService.deleteUniversityByUniversityIds(universityIds));
    }
}
