package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 院校信息对象 university
 * 
 * @author ruoyi
 * @date 2021-10-13
 */
public class University extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 院校编号 */
    @Excel(name = "院校编号")
    private Long universityId;

    /** 院校名称 */
    @Excel(name = "院校名称")
    private String universityName;

    /** 院校分数线 */
    @Excel(name = "院校分数线")
    private String score;

    public void setUniversityId(Long universityId) 
    {
        this.universityId = universityId;
    }

    public Long getUniversityId() 
    {
        return universityId;
    }
    public void setUniversityName(String universityName) 
    {
        this.universityName = universityName;
    }

    public String getUniversityName() 
    {
        return universityName;
    }
    public void setScore(String score) 
    {
        this.score = score;
    }

    public String getScore() 
    {
        return score;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("universityId", getUniversityId())
            .append("universityName", getUniversityName())
            .append("score", getScore())
            .toString();
    }
}
