package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 高考资讯模块对象 message
 * 
 * @author ruoyi
 * @date 2021-10-13
 */
public class Message extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 高考资讯编号 */
    @Excel(name = "高考资讯编号")
    private Integer messageId;

    /** 高考资讯内容 */
    @Excel(name = "高考资讯内容")
    private String messageContent;

    public void setMessageId(Integer messageId) 
    {
        this.messageId = messageId;
    }

    public Integer getMessageId() 
    {
        return messageId;
    }
    public void setMessageContent(String messageContent) 
    {
        this.messageContent = messageContent;
    }

    public String getMessageContent() 
    {
        return messageContent;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("messageId", getMessageId())
            .append("messageContent", getMessageContent())
            .toString();
    }
}
