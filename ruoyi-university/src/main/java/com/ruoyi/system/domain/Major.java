package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 专业信息对象 major
 * 
 * @author ruoyi
 * @date 2021-10-13
 */
public class Major extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 专业编号 */
    @Excel(name = "专业编号")
    private Integer majorId;

    /** 专业名称 */
    @Excel(name = "专业名称")
    private String majorName;

    public void setMajorId(Integer majorId) 
    {
        this.majorId = majorId;
    }

    public Integer getMajorId() 
    {
        return majorId;
    }
    public void setMajorName(String majorName) 
    {
        this.majorName = majorName;
    }

    public String getMajorName() 
    {
        return majorName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("majorId", getMajorId())
            .append("majorName", getMajorName())
            .toString();
    }
}
