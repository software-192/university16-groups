package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Message;

/**
 * 高考资讯模块Service接口
 * 
 * @author ruoyi
 * @date 2021-10-13
 */
public interface IMessageService 
{
    /**
     * 查询高考资讯模块
     * 
     * @param messageId 高考资讯模块主键
     * @return 高考资讯模块
     */
    public Message selectMessageByMessageId(Integer messageId);

    /**
     * 查询高考资讯模块列表
     * 
     * @param message 高考资讯模块
     * @return 高考资讯模块集合
     */
    public List<Message> selectMessageList(Message message);

    /**
     * 新增高考资讯模块
     * 
     * @param message 高考资讯模块
     * @return 结果
     */
    public int insertMessage(Message message);

    /**
     * 修改高考资讯模块
     * 
     * @param message 高考资讯模块
     * @return 结果
     */
    public int updateMessage(Message message);

    /**
     * 批量删除高考资讯模块
     * 
     * @param messageIds 需要删除的高考资讯模块主键集合
     * @return 结果
     */
    public int deleteMessageByMessageIds(Integer[] messageIds);

    /**
     * 删除高考资讯模块信息
     * 
     * @param messageId 高考资讯模块主键
     * @return 结果
     */
    public int deleteMessageByMessageId(Integer messageId);
}
