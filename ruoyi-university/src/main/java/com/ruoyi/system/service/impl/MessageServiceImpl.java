package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MessageMapper;
import com.ruoyi.system.domain.Message;
import com.ruoyi.system.service.IMessageService;

/**
 * 高考资讯模块Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-10-13
 */
@Service
public class MessageServiceImpl implements IMessageService 
{
    @Autowired
    private MessageMapper messageMapper;

    /**
     * 查询高考资讯模块
     * 
     * @param messageId 高考资讯模块主键
     * @return 高考资讯模块
     */
    @Override
    public Message selectMessageByMessageId(Integer messageId)
    {
        return messageMapper.selectMessageByMessageId(messageId);
    }

    /**
     * 查询高考资讯模块列表
     * 
     * @param message 高考资讯模块
     * @return 高考资讯模块
     */
    @Override
    public List<Message> selectMessageList(Message message)
    {
        return messageMapper.selectMessageList(message);
    }

    /**
     * 新增高考资讯模块
     * 
     * @param message 高考资讯模块
     * @return 结果
     */
    @Override
    public int insertMessage(Message message)
    {
        return messageMapper.insertMessage(message);
    }

    /**
     * 修改高考资讯模块
     * 
     * @param message 高考资讯模块
     * @return 结果
     */
    @Override
    public int updateMessage(Message message)
    {
        return messageMapper.updateMessage(message);
    }

    /**
     * 批量删除高考资讯模块
     * 
     * @param messageIds 需要删除的高考资讯模块主键
     * @return 结果
     */
    @Override
    public int deleteMessageByMessageIds(Integer[] messageIds)
    {
        return messageMapper.deleteMessageByMessageIds(messageIds);
    }

    /**
     * 删除高考资讯模块信息
     * 
     * @param messageId 高考资讯模块主键
     * @return 结果
     */
    @Override
    public int deleteMessageByMessageId(Integer messageId)
    {
        return messageMapper.deleteMessageByMessageId(messageId);
    }
}
