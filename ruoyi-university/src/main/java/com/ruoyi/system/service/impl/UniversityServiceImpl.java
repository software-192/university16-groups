package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UniversityMapper;
import com.ruoyi.system.domain.University;
import com.ruoyi.system.service.IUniversityService;

/**
 * 院校信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-10-13
 */
@Service
public class UniversityServiceImpl implements IUniversityService 
{
    @Autowired
    private UniversityMapper universityMapper;

    /**
     * 查询院校信息
     * 
     * @param universityId 院校信息主键
     * @return 院校信息
     */
    @Override
    public University selectUniversityByUniversityId(Long universityId)
    {
        return universityMapper.selectUniversityByUniversityId(universityId);
    }

    /**
     * 查询院校信息列表
     * 
     * @param university 院校信息
     * @return 院校信息
     */
    @Override
    public List<University> selectUniversityList(University university)
    {
        return universityMapper.selectUniversityList(university);
    }

    /**
     * 新增院校信息
     * 
     * @param university 院校信息
     * @return 结果
     */
    @Override
    public int insertUniversity(University university)
    {
        return universityMapper.insertUniversity(university);
    }

    /**
     * 修改院校信息
     * 
     * @param university 院校信息
     * @return 结果
     */
    @Override
    public int updateUniversity(University university)
    {
        return universityMapper.updateUniversity(university);
    }

    /**
     * 批量删除院校信息
     * 
     * @param universityIds 需要删除的院校信息主键
     * @return 结果
     */
    @Override
    public int deleteUniversityByUniversityIds(Long[] universityIds)
    {
        return universityMapper.deleteUniversityByUniversityIds(universityIds);
    }

    /**
     * 删除院校信息信息
     * 
     * @param universityId 院校信息主键
     * @return 结果
     */
    @Override
    public int deleteUniversityByUniversityId(Long universityId)
    {
        return universityMapper.deleteUniversityByUniversityId(universityId);
    }
}
