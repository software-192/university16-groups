package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.University;

/**
 * 院校信息Service接口
 * 
 * @author ruoyi
 * @date 2021-10-13
 */
public interface IUniversityService 
{
    /**
     * 查询院校信息
     * 
     * @param universityId 院校信息主键
     * @return 院校信息
     */
    public University selectUniversityByUniversityId(Long universityId);

    /**
     * 查询院校信息列表
     * 
     * @param university 院校信息
     * @return 院校信息集合
     */
    public List<University> selectUniversityList(University university);

    /**
     * 新增院校信息
     * 
     * @param university 院校信息
     * @return 结果
     */
    public int insertUniversity(University university);

    /**
     * 修改院校信息
     * 
     * @param university 院校信息
     * @return 结果
     */
    public int updateUniversity(University university);

    /**
     * 批量删除院校信息
     * 
     * @param universityIds 需要删除的院校信息主键集合
     * @return 结果
     */
    public int deleteUniversityByUniversityIds(Long[] universityIds);

    /**
     * 删除院校信息信息
     * 
     * @param universityId 院校信息主键
     * @return 结果
     */
    public int deleteUniversityByUniversityId(Long universityId);
}
